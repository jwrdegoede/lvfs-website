#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use,no-member,too-few-public-methods

import os
import zlib
import tempfile
from collections import defaultdict
from typing import Dict, Optional, Tuple, List

from uefi_r2 import UefiAnalyzer, UefiRule, UefiScanner

from lvfs import db
from lvfs.pluginloader import (
    PluginBase,
    PluginError,
    PluginSettingBool,
    PluginSettingList,
)
from lvfs.firmware.models import Firmware
from lvfs.tests.models import Test
from lvfs.components.models import Component, ComponentShard, ComponentShardAttribute


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "uefi_scanner")
        self.name = "UEFI R2"
        self.summary = "Decompile the EFI binary and scan for known issues"
        self.order_after = ["uefi-extract"]
        self.settings.append(
            PluginSettingBool(key="uefi_scanner_enabled", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingBool(
                key="uefi_scanner_attrs", name="Add shard attributes", default=False
            )
        )
        self.settings.append(
            PluginSettingBool(
                key="uefi_scanner_sandbox",
                name="Sandbox using Bubblewrap",
                default=False,
            )
        )
        self.settings.append(
            PluginSettingList(
                key="uefi_scanner_rules",
                name="Rule locations",
                default=["plugins/uefi_scanner/rules/thinkpwn.yml"],
            )
        )

    def require_test_for_md(self, md):

        # only run for capsule updates
        if not md.protocol:
            return False
        if not md.blob:
            return False
        return md.protocol.value == "org.uefi.capsule"

    def _require_test_for_fw(self, fw: Firmware) -> bool:
        for md in fw.mds:
            if self.require_test_for_md(md):
                return True
        return False

    def ensure_test_for_fw(self, fw):

        # add if not already exists
        if not self._require_test_for_fw(fw):
            return
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def _run_test_on_shard(self, test: Test, shard: ComponentShard) -> None:

        # check if suitable
        if not shard.blob:
            return
        if shard.blob[0:2] != b"MZ":
            return

        # delete old attributes
        for attr in [attr for attr in shard.attributes if attr.plugin_id == self.id]:
            shard.attributes.remove(attr)

        # analyze
        print("working on {}…".format(shard.name))
        unique_attrs: Dict[str, List[Tuple[str, Optional[str]]]] = defaultdict(list)
        with tempfile.NamedTemporaryFile(
            mode="wb", prefix="uefi_scanner_", suffix=".efi", dir=None, delete=True
        ) as f:
            f.write(shard.blob)
            f.flush()

            # decompile it and discover properties
            radare2home = None
            if self.get_setting_bool("uefi_scanner_sandbox"):
                radare2home = os.path.dirname(os.path.realpath(__file__))
            uefi_analyzer = UefiAnalyzer(image_path=f.name, radare2home=radare2home)

            # optionally add properties
            if self.get_setting_bool("uefi_scanner_attrs"):
                for service in uefi_analyzer.boot_services:
                    if service.name:
                        unique_attrs["BootService"].append((service.name, None))
                for service in uefi_analyzer.runtime_services:
                    if service.name:
                        unique_attrs["RuntimeService"].append((service.name, None))
                for protocol_guid in uefi_analyzer.protocol_guids:
                    if protocol_guid.value:
                        unique_attrs["ProtocolGuid"].append(
                            (protocol_guid.value.lower(), protocol_guid.name)
                        )
                for protocol in uefi_analyzer.protocols:
                    if protocol.value:
                        unique_attrs["Protocol"].append(
                            (protocol.value.lower(), protocol.name)
                        )
                for nvram_var in uefi_analyzer.nvram_vars:
                    if nvram_var.guid:
                        unique_attrs["NvramVar"].append(
                            (nvram_var.guid.lower(), nvram_var.name)
                        )

            for rule_fn in self.get_setting("uefi_scanner_rules", required=True).split(
                ","
            ):
                if not os.path.isfile(rule_fn):
                    test.add_fail(shard.name, "Cannot load {}".format(rule_fn))
                    continue
                uefi_rule = UefiRule(rule_fn)
                if not uefi_rule.name:
                    test.add_fail(shard.name, "No name found for {}".format(rule_fn))
                    continue
                scanner = UefiScanner(uefi_analyzer, uefi_rule)
                if scanner.result:
                    msg = "A potential security issue was detected in {} ({}).".format(
                        shard.name, shard.guid
                    )
                    if uefi_rule.url:
                        msg += " Please see {} for more information.".format(
                            uefi_rule.url
                        )
                    test.add_fail(uefi_rule.name, msg)

        # add attrs
        for key in unique_attrs:
            for value, comment in set(unique_attrs[key]):
                shard.attributes.append(
                    ComponentShardAttribute(
                        plugin_id=self.id, key=key, value=value, comment=comment
                    )
                )

    def run_test_on_md(self, test: Test, md: Component) -> None:

        # run analysis on each shard
        for shard in md.shards:
            self._run_test_on_shard(test, shard)


# run with PYTHONPATH=. ./env/bin/python3 plugins/uefi_scanner/__init__.py
if __name__ == "__main__":
    import sys

    plugin = Plugin()
    _test = Test(plugin_id=plugin.id)
    _md = Component()
    try:
        for fn in sys.argv[1:]:
            _shard = ComponentShard(name=fn)
            with open(fn, "rb") as _f:
                try:
                    _shard.blob = zlib.decompressobj().decompress(_f.read())
                except zlib.error:
                    _f.seek(0)
                    _shard.blob = _f.read()
            _md.shards.append(_shard)
    except IndexError:
        _shard = ComponentShard()
        _shard.blob = (
            b"MZxxx\x4D\x95\x90\x13\x95\xDA\x27\x42\x93\x28\x72\x82\xC2\x17\xDA\xA8xxx"
        )
        _md.shards.append(_shard)
    plugin.run_test_on_md(_test, _md)
    for attribute in _test.attributes:
        print(attribute)
    for _shard in _md.shards:
        print("\n{}:".format(_shard.name))
        for _attr in _shard.attributes:
            print(_attr.key, _attr.value, _attr.comment or "")
