#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

from typing import Optional

from sqlalchemy import (
    Column,
    Boolean,
    ForeignKey,
    Integer,
    Text,
)
from sqlalchemy.orm import relationship

from lvfs import db


class Upload(db.Model):

    __tablename__ = "uploads"

    upload_id = Column(Integer, primary_key=True)
    vendor_id = Column(Integer, ForeignKey("vendors.vendor_id"), nullable=False)
    firmware_revision_id = Column(
        Integer, ForeignKey("firmware_revisions.firmware_revision_id"), nullable=False
    )
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    addr = Column(Text, nullable=False)
    target = Column(Text, nullable=False)
    auto_delete = Column(Boolean, default=False)
    _status = Column("status", Text, default=None)

    user = relationship("User", foreign_keys=[user_id])
    vendor = relationship("Vendor", foreign_keys=[vendor_id])
    revision = relationship("FirmwareRevision", foreign_keys=[firmware_revision_id])

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status: Optional[str]) -> None:
        self._status = status
        db.session.commit()

    def __repr__(self) -> str:
        return "Upload object %s" % self.upload_id
