"""

Revision ID: dfa8440c4f38
Revises: 57ff8594b076
Create Date: 2021-06-08 22:19:14.351258

"""

# revision identifiers, used by Alembic.
revision = "dfa8440c4f38"
down_revision = "57ff8594b076"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "firmware_revisions",
        sa.Column("firmware_revision_id", sa.Integer(), nullable=False),
        sa.Column("firmware_id", sa.Integer(), nullable=False),
        sa.Column("filename", sa.Text(), nullable=False),
        sa.Column("timestamp", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["firmware_id"],
            ["firmware.firmware_id"],
        ),
        sa.PrimaryKeyConstraint("firmware_revision_id"),
    )
    op.create_index(
        op.f("ix_firmware_revisions_filename"),
        "firmware_revisions",
        ["filename"],
        unique=True,
    )
    op.create_index(
        op.f("ix_firmware_revisions_firmware_id"),
        "firmware_revisions",
        ["firmware_id"],
        unique=False,
    )
    op.alter_column("firmware", "filename", existing_type=sa.TEXT(), nullable=True)
    op.drop_index("ix_firmware_filename", table_name="firmware")


def downgrade():
    op.create_index("ix_firmware_filename", "firmware", ["filename"], unique=False)
    op.alter_column("firmware", "filename", existing_type=sa.TEXT(), nullable=False)
    op.drop_index(
        op.f("ix_firmware_revisions_firmware_id"), table_name="firmware_revisions"
    )
    op.drop_index(
        op.f("ix_firmware_revisions_filename"), table_name="firmware_revisions"
    )
    op.drop_table("firmware_revisions")
