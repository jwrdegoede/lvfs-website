"""

Revision ID: 2a480a3835c8
Revises: 53a9eb23a4de
Create Date: 2021-06-14 11:35:16.883468

"""

# revision identifiers, used by Alembic.
revision = "2a480a3835c8"
down_revision = "53a9eb23a4de"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index(
        op.f("ix_component_shard_attributes_key"),
        "component_shard_attributes",
        ["key"],
        unique=False,
    )
    op.create_index(
        op.f("ix_component_shard_attributes_plugin_id"),
        "component_shard_attributes",
        ["plugin_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_component_shard_attributes_value"),
        "component_shard_attributes",
        ["value"],
        unique=False,
    )


def downgrade():
    op.drop_index(
        op.f("ix_component_shard_attributes_value"),
        table_name="component_shard_attributes",
    )
    op.drop_index(
        op.f("ix_component_shard_attributes_plugin_id"),
        table_name="component_shard_attributes",
    )
    op.drop_index(
        op.f("ix_component_shard_attributes_key"),
        table_name="component_shard_attributes",
    )
