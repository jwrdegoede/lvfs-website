"""

Revision ID: ad7fdc1f8011
Revises: 0055ad62fec4
Create Date: 2021-06-21 20:40:26.740228

"""

# revision identifiers, used by Alembic.
revision = "ad7fdc1f8011"
down_revision = "0055ad62fec4"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("requirements", sa.Column("hardness", sa.Text(), nullable=True))


def downgrade():
    op.drop_column("requirements", "hardness")
