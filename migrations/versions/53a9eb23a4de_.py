"""

Revision ID: 53a9eb23a4de
Revises: 5d8a17f49e00
Create Date: 2021-06-14 10:50:56.352326

"""

# revision identifiers, used by Alembic.
revision = "53a9eb23a4de"
down_revision = "5d8a17f49e00"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index(
        op.f("ix_component_shards_plugin_id"),
        "component_shards",
        ["plugin_id"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_component_shards_plugin_id"), table_name="component_shards")
