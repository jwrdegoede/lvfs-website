"""

Revision ID: 0055ad62fec4
Revises: 2a480a3835c8
Create Date: 2021-06-20 20:39:01.184938

"""

# revision identifiers, used by Alembic.
revision = "0055ad62fec4"
down_revision = "2a480a3835c8"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "vendor_assets",
        sa.Column("vendor_asset_id", sa.Integer(), nullable=False),
        sa.Column("vendor_id", sa.Integer(), nullable=False),
        sa.Column("filename", sa.Text(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("vendor_asset_id"),
    )
    op.create_index(
        op.f("ix_vendor_assets_vendor_id"), "vendor_assets", ["vendor_id"], unique=False
    )


def downgrade():
    op.drop_index(op.f("ix_vendor_assets_vendor_id"), table_name="vendor_assets")
    op.drop_table("vendor_assets")
