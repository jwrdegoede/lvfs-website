"""

Revision ID: 8dc3e79088f7
Revises: c449435d60fb
Create Date: 2021-09-26 10:59:05.108019

"""

# revision identifiers, used by Alembic.
revision = "8dc3e79088f7"
down_revision = "c449435d60fb"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("yara_query", sa.Column("kind", sa.Text(), nullable=True))
    op.add_column("yara_query", sa.Column("title", sa.Text(), nullable=True))


def downgrade():
    op.drop_column("yara_query", "kind")
    op.drop_column("yara_query", "title")
