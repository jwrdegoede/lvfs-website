"""

Revision ID: 305ce4dd953d
Revises: fb0f457ea76c
Create Date: 2021-05-13 16:26:44.336567

"""

# revision identifiers, used by Alembic.
revision = '305ce4dd953d'
down_revision = 'fb0f457ea76c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('vendors', sa.Column('psirt_url', sa.Text(), nullable=True))


def downgrade():
    op.drop_column('vendors', 'psirt_url')
